# zynthia-project

Built with [Astro](https://astro.build/) using the official [Starlight](https://starlight.astro.build/) docs template.

Builds the website located here: https://zynthia-gardener.codeberg.page/