
---

If you spot any mistakes, want to give input on accessibility, plan to do translations, or wish to tell me that I'm awesome, please contact me! You can contact me via
  - [Mastodon](https://kolektiva.social/@zynthiagardener)
  - Email, which can be found on [my YouTube about page](https://www.youtube.com/@ZynthiaProject/about) (does not have to be "For business inquiries")
  - [Anonymous Tally form](https://tally.so/r/mYRqRv)

Copyright 2023 Zynthia Gardener.  
The Zynthia Project logo and the names "Zynthia Project" and "Zynthia Gardener" are not licensed under CC BY-SA.  
Except where otherwise noted, the content of this project is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.  
The code generating this website is licensed under the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.html) or later.