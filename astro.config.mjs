import { defineConfig } from "astro/config";
import starlight from "@astrojs/starlight";
import remarkMath from "remark-math";
import { visit } from "unist-util-visit";

// rehypeMath
/** @type {import('unified').Plugin<[], import('hast').Root>} */
function rehypeMath() {
	return (tree) => {
		visit(tree, "element", (node) => {
			const classes =
				node.properties && Array.isArray(node.properties.className)
					? node.properties.className
					: [];
			const inline = classes.includes("math-inline");
			const display = classes.includes("math-display");

			if (inline || display) {
				//console.log(node);
				const SIGNAL_CLASS = "raw-latex-wrapper";
				if (classes.includes(SIGNAL_CLASS)) {
					return;
				}
				node.properties.className.push(SIGNAL_CLASS);
				if (inline) {
					node.tagName = "code";
					node.children[0].value = "$" + node.children[0].value + "$";
				} else {
					node.tagName = "pre";
					node.children[0].value =
						"\\[" + node.children[0].value + "\\]";
					const newChild = {
						type: "element",
						tagName: "code",
						properties: { className: node.properties.className },
						children: [node.children[0]],
					};
					node.children = [newChild];
				}
			}
		});
	};
}

// https://astro.build/config
export default defineConfig({
	integrations: [
		starlight({
			title: "Zynthia Project",
			social: {
				github: "https://codeberg.org/zynthia-gardener/zynthia-project",
				mastodon: "https://kolektiva.social/@zynthiagardener",
			},
			customCss: ["/src/styles.css", "@fontsource/atkinson-hyperlegible"],
			logo: {
				light: "/src/assets/logo-light.svg",
				dark: "/src/assets/logo-dark.svg",
			},
			head: [
				{
					tag: "link",
					attrs: { rel: "icon", href: "/favicon.ico", sizes: "any" },
				},
				{
					tag: "link",
					attrs: {
						rel: "apple-touch-icon",
						href: "/apple-touch-icon.png",
					},
				},
				{
					tag: "link",
					attrs: { rel: "manifest", href: "/site.webmanifest" },
				},
			],
			sidebar: [
				{
					label: "Zynthia Project",
					items: [
						{ label: "Home", link: "/" },
						{ label: "Purpose", link: "/purpose/" },
						{ label: "Resource Library", link: "/library/" },
						{
							label: "Learn",
							autogenerate: { directory: "learn" },
						},
						{
							label: "Blog",
							autogenerate: { directory: "blog" },
						},
					],
				},
			],
		}),
	],
	markdown: {
		remarkPlugins: [remarkMath],
		rehypePlugins: [rehypeMath],
	},
});
